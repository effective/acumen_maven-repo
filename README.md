# Acumen Dependencies Maven Repository

This repository contains dependencies, required to build Acumen, that are not currently available in a public Maven repository.

## Adding a dependency

Using [script](rep-maker.py) developed by [Neil Rubens](https://bitbucket.org/neil_rubens/rapidminer_maven-repo/wiki/Home):

1. Add jar file(s) to subdirectory of jars/ matching base package name of dependency (e.g. jars/com.mydomain/mydep.jar, where the base package of classes in mydep.jar is com.mydomain).
2. Run rep-maker.py, passing full path to new jar file(s) and version number of dependency (e.g. ./rep-maker.py /PATH/TO/jars/com.mydomain 1.0).
3. Make the generated file mvn_commands executable and run it (chmod +x mvn_commands; mvn_commands). This will create directories and files for the new dependency under the repository/ directory.
4. Add repository/ to git and commit.
5. Add the new dependency to build.sbt.